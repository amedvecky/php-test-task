<?php

/**
 * Class Database
 *  provides interface to database
 */
class Database {

    /**
     * @var Configuration
     */
    private Configuration $configuration;

    /**
     * Database constructor.
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration) {
        $this->configuration = $configuration;
    }

    /**
     * @return PDO
     */
    public function getConnection() : PDO {
        $dsn = 'mysql:host='
            . $this->configuration->db_host
            . ';dbname='
            . $this->configuration->db_name
            . ';charset=utf8';

        try {
            $db = new PDO($dsn, $this->configuration->db_user, $this->configuration->db_pass);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $db;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }
}