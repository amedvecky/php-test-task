<?php

require_once 'Product.php';

/**
 * Class Furniture
 */
class Furniture extends Product {

    /**
     * Furniture's height in cm
     * @var int
     */
    private int $height;
    /**
     * Furniture's width in cm
     * @var int
     */
    private int $width;
    /**
     * Furniture's length in cm
     * @var int
     */
    private int $length;


    /**
     * Furniture constructor.
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int $height
     * @param int $width
     * @param int $length
     */
    public function __construct(string $sku, string $name, float $price, int $height, int $width, int $length) {
        parent::__construct($sku, $name, $price);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    /**
     * @return int
     */
    public function getHeight(): int {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth(): int {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getLength(): int {
        return $this->length;
    }

    /**
     * Saves Furniture object in database
     * @return bool
     */
    protected function validate(): bool {
        return parent::validate() && !$this->height === false && !$this->width === false && !$this->length === false;
    }


    /**
     * @param PDO $databaseConnection
     * @return bool
     */
    public function saveProduct(PDO $databaseConnection): bool {
        if ($this->validate()) {

            $sql = "INSERT INTO furniture (sku, name, price, height, width, length)
            VALUES (:sku, :name, :price, :height, :width, :length)";

            $stmt = $databaseConnection->prepare($sql);

            $stmt->bindValue(':sku', $this->sku, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':price',$this->price, PDO::PARAM_STR);
            $stmt->bindValue(':height',$this->height, PDO::PARAM_INT);
            $stmt->bindValue(':width',$this->width, PDO::PARAM_INT);
            $stmt->bindValue(':length',$this->length, PDO::PARAM_INT);

            if ($stmt->execute()) {
                return true;
            } else {
                echo "ERROR: " . $stmt->errorInfo();
                return false;
            }

        } else {
            echo 'Validation error';
            return false;
        }
    }
    /**
     * @param PDO $databaseConnection
     * array of Furniture objects
     * @return iterable
     */
    public static function getAllProducts(PDO $databaseConnection): iterable {
        $sql = "SELECT *
                FROM furniture";

        $stmt = $databaseConnection->prepare($sql);

        if ($stmt->execute()) {
            $products = $stmt->fetchAll(PDO::FETCH_OBJ);
            $results = array();
            foreach ($products as $product) {
                $furniture = new Furniture(
                    $product->sku,
                    $product->name,
                    $product->price,
                    $product->height,
                    $product->width,
                    $product->length);
                $furniture->setId($product->id);
                $results[] = $furniture;
            }
            return $results;
        }

        echo "ERROR: " . $stmt->errorInfo();
        return null;
    }

    /**
     *  array of product ids for deletion
     * @param array $ids
     * @param PDO $databaseConnection
     */
    public static function deleteProductsByIds(array $ids, PDO $databaseConnection) {
        foreach ($ids as $key => $value) {
            $ids[$key] = "'".$value."'";
        }
        $sql = "DELETE FROM furniture 
                WHERE id IN(" . implode(",", $ids) .")";
        $stmt = $databaseConnection->prepare($sql);
        if ($stmt->execute()) {
            return;
        }
        echo "ERROR: " . $stmt->errorInfo();
    }
}