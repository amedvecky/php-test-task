<?php

require_once 'Product.php';

/**
 * Class Book
 *
 */
class Book extends Product {
    /**
     * Book's weight in KG
     * @var int
     */
    private int $weight;

    /**
     * Book constructor.
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int $weight
     */
    public function __construct(string $sku, string $name, float $price, int $weight) {
        parent::__construct($sku, $name, $price);
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getWeight(): int {
        return $this->weight;
    }


    /**
     *  Validates Book's properties, all properties must not be empty
     * @return bool
     */
    protected function validate(): bool {
        return parent::validate() && !$this->weight === false;
    }


    /**
     *  Saves Book object to database
     * @param PDO $databaseConnection
     * @return bool
     */
    public function saveProduct(PDO $databaseConnection): bool {
        if ($this->validate()) {

            $sql = "INSERT INTO books (sku, name, price, weight)
            VALUES (:sku,:name, :price, :weight)";

            $stmt = $databaseConnection->prepare($sql);

            $stmt->bindValue(':sku', $this->sku, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':price',$this->price, PDO::PARAM_STR);
            $stmt->bindValue(':weight',$this->weight, PDO::PARAM_INT);

            if ($stmt->execute()) {
                return true;
            } else {
                echo "ERROR: " . $stmt->errorInfo();
                return false;
            }

        } else {
            echo 'Validation error';
            return false;
        }
    }

    /**
     * @param PDO $databaseConnection
     * array of Book objects
     * @return iterable
     */
    public static function getAllProducts(PDO $databaseConnection): iterable {
        $sql = "SELECT *
                FROM books";

        $stmt = $databaseConnection->prepare($sql);

        if ($stmt->execute()) {
            $products = $stmt->fetchAll(PDO::FETCH_OBJ);
            $results = array();
            foreach ($products as $product) {
                $book = new Book($product->sku, $product->name, $product->price, $product->weight);
                $book->setId($product->id);
                $results[] = $book;
            }
            return $results;
        }

        echo "ERROR: " . $stmt->errorInfo();
        return null;
    }

    /**
     * array of product ids for deletion
     * @param array $ids
     * @param PDO $databaseConnection
     */
    public static function deleteProductsByIds(array $ids, PDO $databaseConnection) {
        foreach ($ids as $key => $value) {
            $ids[$key] = "'".$value."'";
        }
        $sql = "DELETE FROM books 
                WHERE id IN(" . implode(",", $ids) .")";
        $stmt = $databaseConnection->prepare($sql);
        if ($stmt->execute()) {
            return;
        }
        echo "ERROR: " . $stmt->errorInfo();
    }
}