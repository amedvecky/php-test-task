<?php

require_once 'Product.php';

/**
 * Class CD
 */
class CD extends Product {
    /**
     * CD size in MB
     * @var int
     */
    private int $size;

    /**
     * CD constructor.
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int $size
     */
    public function __construct(string $sku, string $name, float $price, int $size) {
        parent::__construct($sku, $name, $price);
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getSize(): int {
        return $this->size;
    }


    /**
     * Validates CD's properties, all properties must not be empty
     * @return bool
     */
    protected function validate(): bool {
        return parent::validate() && !$this->size === false;
    }


    /**
     * Saves CD object in database
     * @param PDO $databaseConnection
     * @return bool
     */
    public function saveProduct(PDO $databaseConnection): bool {
        if ($this->validate()) {

            $sql = "INSERT INTO cd (sku, name, price, size)
            VALUES (:sku,:name, :price, :size)";

            $stmt = $databaseConnection->prepare($sql);

            $stmt->bindValue(':sku', $this->sku, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':price',$this->price, PDO::PARAM_STR);
            $stmt->bindValue(':size',$this->size, PDO::PARAM_INT);

            if ($stmt->execute()) {
                return true;
            } else {
                echo "ERROR: " . $stmt->errorInfo();
                return false;
            }

        } else {
            echo 'Validation error';
            return false;
        }
    }

    /**
     * @param PDO $databaseConnection
     * array of CD objects
     * @return iterable
     */
    public static function getAllProducts(PDO $databaseConnection): iterable {
        $sql = "SELECT *
                FROM cd";

        $stmt = $databaseConnection->prepare($sql);

        if ($stmt->execute()) {
            $products = $stmt->fetchAll(PDO::FETCH_OBJ);
            $results = array();
            foreach ($products as $product) {
                $cd = new CD($product->sku, $product->name, $product->price, $product->size);
                $cd->setId($product->id);
                $results[] = $cd;
            }
            return $results;
        }

        echo "ERROR: " . $stmt->errorInfo();
        return null;
    }

    /**
     *  array of product ids for deletion
     * @param array $ids
     * @param PDO $databaseConnection
     */
    public static function deleteProductsByIds(array $ids, PDO $databaseConnection) {
        foreach ($ids as $key => $value) {
            $ids[$key] = "'".$value."'";
        }
        $sql = "DELETE FROM cd 
                WHERE id IN(" . implode(",", $ids) .")";
        $stmt = $databaseConnection->prepare($sql);
        if ($stmt->execute()) {
            return;
        }
        echo "ERROR: " . $stmt->errorInfo();
    }
}