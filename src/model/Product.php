<?php


/**
 * Class Product
 *  Base class for all product types
 */
abstract class Product {
    /**
     * @var int
     */
    protected int $id;
    /**
     * @var string
     */
    protected string $sku;
    /**
     * @var string
     */
    protected string $name;
    /**
     * @var float
     */
    protected float $price;

    /**
     * Product constructor.
     * @param string $sku
     * @param string $name
     * @param float $price
     */
    public function __construct(string $sku, string $name, float $price) {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSku(): string {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float {
        return $this->price;
    }

    /**
     * @param PDO $databaseConnection
     */
    public function saveProduct(PDO $databaseConnection) {

    }

    /**
     * @return bool
     */
    protected function validate(): bool {
        return !$this->sku == '' && !$this->name == '' && !$this->price === false;
    }
}