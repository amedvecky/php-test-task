<?php
require_once 'view/ProductListHandler.php';
require_once 'includes/header.php';
$productListHandler = new ProductListHandler();
?>
<form method="post" action="actions/delete_product.php">
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-info">
        <a class="navbar-brand" href="#">Product List</a>
        <div class="navbar-nav">
            <a class="nav-link active" href="<?php echo $productListHandler->getAddProductPath() ?>">
                Add Product
            </a>
        </div>
        <div class="nav justify-content-end">
                <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Delete products</button>
        </div>
    </nav>

    <div class="container" style="max-width: 100%; padding-top: 60px">
        <div class="row" style="margin-top: 1%">
            <?php $productListHandler->renderListItems(); ?>
        </div>
    </div>
</form>

<?php require 'includes/footer.php' ?>
