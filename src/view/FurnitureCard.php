<?php

require_once __DIR__ . '/../model/Furniture.php';
require_once 'ProductCard.php';

/**
 * Class FurnitureCard
 *  Renders CD object representation using Bootstrap Card element
 */
class FurnitureCard implements ProductCard {
    /**
     * Furniture object
     * @var Furniture
     */
    private Furniture $product;

    /**
     * ProductCard constructor.
     * @param Furniture $product
     */
    public function __construct(Furniture $product) {
        $this->product = $product;
    }

    /**
     * renders furniture's card using CD object data
     */
    public function render() {
        echo '
   <div class="card border-info mb-3">
     <div class="card-header text-info">' . htmlspecialchars($this->product->getSku()) . '</div>
        <div class="card-body text-info">
         <p class="card-text">' . htmlspecialchars($this->product->getName()) . '</p>
         <p class="card-text">$' . $this->product->getPrice() . '</p>
         <p class="card-text">Dimension: '
            . $this->product->getHeight() . 'x'
            . $this->product->getWidth() . 'x'
            . $this->product->getLength()
            . '</p>
          <p class="card-text">
          <input type="checkbox" name="id[]" value=' . $this->product->getId() . ':Furniture' . '>
                    delete
           </p>
        </div>
   </div>
   ';
    }


}