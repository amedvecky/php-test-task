<?php

require_once __DIR__ . '/../model/CD.php';
require_once 'ProductCard.php';

/**
 * Class CDCard
 *  Renders CD object representation using Bootstrap Card element
 */
class CDCard implements ProductCard {
    /**
     * CD object
     * @var CD
     */
    private CD $product;

    /**
     * ProductCard constructor.
     * @param CD $product
     */
    public function __construct(CD $product) {
        $this->product = $product;
    }

    /**
     * renders CD's card using CD object data
     */
    public function render() {
        echo '
   <div class="card border-info mb-3" xmlns="http://www.w3.org/1999/html">
     <div class="card-header text-info">' . htmlspecialchars($this->product->getSku()) . '</div>
        <div class="card-body text-info">
         <p class="card-text">' . htmlspecialchars($this->product->getName()) . '</p>
         <p class="card-text">' . $this->product->getPrice() . '$</p>
         <p class="card-text"> Size: ' . $this->product->getSize() . ' MB</p>
          <p class="card-text">
          <input type="checkbox" name="id[]" value=' . $this->product->getId() . ':CD' . '>
                    delete
           </p>
        </div>
   </div>
   ';
    }


}