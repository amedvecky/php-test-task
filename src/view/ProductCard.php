<?php

/**
 * Interface ProductCard
 *  Base Type for ProductCard subtypes
 */
interface ProductCard {
    /**
     * Renders product's card using object data
     */
    public function render();
}