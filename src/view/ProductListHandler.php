<?php

require_once __DIR__ . '/../helpers/Configuration.php';
require_once __DIR__ . '/../controller/ProductController.php';
require_once 'ProductCardFactory.php';

/**
 * Class ProductListHandler
 */
class ProductListHandler {
    /**
     * @var int
     */
    private int $MAX_ITEMS_IN_ROW = 4;
    /**
     * @var Configuration
     */
    private Configuration $configuration;

    /**
     * ProductListHandler constructor.
     */
    public function __construct() {
        $this->configuration = new Configuration();
    }


    /**
     * @return string
     */
    public function getAddProductPath() {
        $addProductPath = '/index.php';
        if ($this->configuration->env == 'localhost') {
            $addProductPath = '/php-test-task/src/index.php';
        }
        return $addProductPath;
    }

    /**
     *
     */
    public function renderListItems() {

      $productController = new ProductController();

      $products = $productController->getAllProducts();

      $productCards = array();

      foreach ($products as $product) {
          $productCards[] = ProductCardFactory::getProductCard($product);
      }

      $itemInRowCount = 0;
      foreach ($productCards as $productCard) {
          if ($itemInRowCount < $this->MAX_ITEMS_IN_ROW) {
              echo '<div class="col-sm">';
              $productCard->render();
              echo '</div>';
              $itemInRowCount++;
          } else {
              echo '</div>';
              echo '<div class="row">';
              echo '<div class="col-sm">';
              $productCard->render();
              echo '</div>';
              $itemInRowCount = 1;
          }
      }
  }
}