<?php

require_once __DIR__ . '/../model/Book.php';
require_once 'ProductCard.php';

/**
 * Class BookCard
 *  Renders Book object representation using Bootstrap Card element
 */
class BookCard implements ProductCard {
    /**
     *  Book Object
     * @var Book
     */
    private Book $product;

    /**
     * ProductCard constructor.
     * @param Book $product
     */
    public function __construct(Book $product) {
        $this->product = $product;
    }

    /**
     * renders Book's card using Book object data
     */
    public function render() {
        echo '
   <div class="card border-info mb-3">
     <div class="card-header text-info">' . htmlspecialchars($this->product->getSku()) . '</div>
        <div class="card-body text-info">
         <p class="card-text">' . htmlspecialchars($this->product->getName()) . '</p>
         <p class="card-text">$' . $this->product->getPrice() . '</p>
         <p class="card-text">Weight: ' . $this->product->getWeight() . ' KG</p>
          <p class="card-text">
          <input type="checkbox" name="id[]" value=' . $this->product->getId() . ':Book' . '>
                    delete
           </p>
        </div>
   </div>
   ';
    }


}