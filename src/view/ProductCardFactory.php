<?php

require_once 'ProductCard.php';
require_once 'BookCard.php';
require_once 'CDCard.php';
require_once 'FurnitureCard.php';

/**
 * Class ProductCardFactory
 */
class ProductCardFactory {
    /**
     * @param Product $product
     * @return ProductCard|null
     */
    public static function getProductCard(Product $product): ?ProductCard {
        switch (get_class($product)) {
            case 'Book':
                return new BookCard($product);
            case 'CD':
                return new CDCard($product);
            case 'Furniture':
                return new FurnitureCard($product);
        }
        return null;
    }
}