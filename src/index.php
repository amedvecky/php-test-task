<?php
require 'includes/header.php';
require_once 'helpers/Configuration.php';
$configuration = new Configuration();

$productListPath = '/product_list.php';
if ($configuration->env == 'localhost') {
    $productListPath = '/php-test-task/src/product_list.php';
}
?>
    <body onload="renderOptionalFormPart()">
<nav class="navbar navbar-expand-lg navbar-dark bg-info">
    <a class="navbar-brand" href="#">Add Product</a>
    <div class="navbar-nav">
        <a class="nav-link active" href="<?php echo $productListPath ?>">
            Product List
        </a>
    </div>
</nav>
<div id="root">
    <div class="card border-info mb-3">
        <div class="card-body text-info">
            <form method="post" action="actions/add_product.php">
                <div class="form-group row">
                    <label for="sku" class="col-sm-2 col-form-label">SKU</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="sku" name="sku" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price" class="col-sm-2 col-form-label">Price</label>
                    <div class="col-sm-10">
                        <input type="number" step="0.01" class="form-control" id="price" name="price" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="type" class="col-sm-2 col-form-label">Product Type</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="type" name="type" onchange="renderOptionalFormPart()">
                            <option>CD</option>
                            <option>Furniture</option>
                            <option>Book</option>
                        </select>
                    </div>
                </div>
                <div id="optional">

                </div>
                <button type="submit" class="btn btn-outline-info">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php require 'includes/footer.php' ?>