const cdFormElement = '<div class="form-group row">\n' +
    '                        <label for="size" class="col-sm-2 col-form-label">Size</label>\n' +
    '                        <div class="col-sm-10">\n' +
    '                            <input type="number" class="form-control" id="size" name="size" required>\n' +
    '                        </div>\n' +
    '                        <div class="col-sm-10">\n' +
    '                            <small class="form-text text-muted">\n' +
    '                                Enter CD size in MB\n' +
    '                            </small>\n' +
    '                        </div>\n' +
    '                    </div>';

const bookFormElement = '<div class="form-group row">\n' +
    '                        <label for="weight" class="col-sm-2 col-form-label">Weight</label>\n' +
    '                        <div class="col-sm-10">\n' +
    '                            <input type="number" class="form-control" id="weight" name="weight" required>\n' +
    '                        </div>\n' +
    '                        <div class="col-sm-10">\n' +
    '                            <small class="form-text text-muted">\n' +
    '                                Enter book\'s weight in Kg.\n' +
    '                            </small>\n' +
    '                        </div>\n' +
    '                    </div>';

const furnitureFormElement = '<div class="form-group">\n' +
    '                        <div class="form-group row">\n' +
    '                            <label for="height" class="col-sm-2 col-form-label">Height</label>\n' +
    '                            <div class="col-sm-10">\n' +
    '                                <input type="number" class="form-control" id="height" name="height" required>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="form-group row">\n' +
    '                            <label for="width" class="col-sm-2 col-form-label">Width</label>\n' +
    '                            <div class="col-sm-10">\n' +
    '                                <input type="number" class="form-control" id="width" name="width" required>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="form-group row">\n' +
    '                            <label for="length" class="col-sm-2 col-form-label">Length</label>\n' +
    '                            <div class="col-sm-10">\n' +
    '                                <input type="number" class="form-control" id="length" name="length" required>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col-sm-10">\n' +
    '                            <small class="form-text text-muted">\n' +
    '                                Enter furniture params Height x Width x Length\n' +
    '                            </small>\n' +
    '                        </div>\n' +
    '                    </div>';


function renderOptionalFormPart() {
    const element = document.getElementById('optional');
    const type = document.getElementById('type').value;
    switch (type) {
        case 'CD':
            element.innerHTML = cdFormElement;
            break;
        case 'Book':
            element.innerHTML = bookFormElement;
            break;
        case 'Furniture':
            element.innerHTML = furnitureFormElement;
    }
}