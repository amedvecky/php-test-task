<?php


/**
 * Class Configuration
 * configuration storage and provider
 */
class Configuration {
    /**
     * @var mixed|string
     */
    public string $db_host = "127.0.0.1";
    /**
     * @var string
     */
    public string $db_name = "products";
    /**
     * @var mixed|string
     */
    public string $db_user = "root";
    /**
     * @var mixed|string
     */
    public string $db_pass = "example";

    public string $env = "localhost";


    /**
     * Configuration constructor.
     */
    public function __construct() {
        if (getenv('DB_HOST')) {
            $this->db_host = $_ENV['DB_HOST'];
        }

        if (getenv('DB_USER')) {
            $this->db_user = $_ENV['DB_USER'];
        }

        if (getenv('DB_PASSWORD')) {
            $this->db_pass = $_ENV['DB_PASSWORD'];
        }

        if(getenv('ENV')) {
            $this->env = $_ENV['ENV'];
        }
    }
}