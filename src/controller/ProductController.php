<?php

require_once __DIR__ . '/../model/CD.php';
require_once __DIR__ . '/../model/Book.php';
require_once __DIR__ . '/../model/Furniture.php';
require_once __DIR__ . '/../model/Database.php';
require_once __DIR__ . '/../helpers/Configuration.php';
require_once 'ProductFactory.php';



/**
 * Class ProductController
 */
class ProductController {

    /**
     * @var array|string[]
     */
    private static array $productTypes = ['CD', 'Book', 'Furniture'];

    private PDO $databaseConnection;
    private Configuration $configuration;

    /**
     * ProductController constructor.
     */
    public function __construct() {
        $this->configuration = new Configuration();
        $database = new Database($this->configuration);
        $this->databaseConnection = $database->getConnection();
    }


    /**
     * @return iterable
     */
    public function getAllProducts(): iterable {
        $products = array();
        foreach (ProductController::$productTypes as $productType) {
            switch ($productType) {
                case 'CD':
                    $products = array_merge($products, CD::getAllProducts($this->databaseConnection));
                    break;
                case 'Book':
                    $products = array_merge($products, Book::getAllProducts($this->databaseConnection));
                    break;
                case 'Furniture':
                    $products = array_merge($products, Furniture::getAllProducts($this->databaseConnection));
            }
        }
        return $products;
    }

    /**
     *  array of product ids for deletion
     * @param array $idsTypes
     */
    public function deleteProductsByIds(array $idsTypes) {
        $cdIds = array();
        $bookIds = array();
        $furnitureIds = array();
        foreach ($idsTypes as $idType) {
            list($id, $type) = explode(':', $idType);
            switch ($type) {
                case 'CD':
                    $cdIds[] = $id;
                    break;
                case 'Book':
                    $bookIds[] = $id;
                    break;
                case 'Furniture' :
                    $furnitureIds[] = $id;
            }
        }
        if (count($cdIds) > 0) {
            CD::deleteProductsByIds($cdIds, $this->databaseConnection);
        }
        if (count($bookIds) > 0) {
            Book::deleteProductsByIds($bookIds, $this->databaseConnection);
        }
        if (count($furnitureIds) > 0) {
            Furniture::deleteProductsByIds($furnitureIds, $this->databaseConnection);
        }
    }

    public function addProductFormHandler() {
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];

        $product = null;
        $type = $_POST['type'];

        switch ($type) {
            case 'CD' :
                $size = $_POST['size'];
                $product = ProductFactory::getProduct($type, $sku, $name, $price, $size);
                break;
            case 'Book':
                $weight = $_POST['weight'];
                $product = ProductFactory::getProduct($type, $sku, $name, $price, $weight);
                break;
            case 'Furniture' :
                $height = $_POST['height'];
                $width = $_POST['width'];
                $length = $_POST['length'];
                $product = ProductFactory::getProduct($type, $sku, $name, $price, $height, $width, $length);
        }

        $product->saveProduct($this->databaseConnection);

        if ($this->configuration->env == 'localhost') {
            header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php-test-task/src/index.php");
        } else {
            header("Location: http://" . $_SERVER['HTTP_HOST'] . "/index.php");
        }
    }

    public function deleteProductFormHandler() {
        if (isset($_POST['id'])) {
            $this->deleteProductsByIds($_POST['id']);
        }


        if ($this->configuration->env == 'localhost') {
            header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php-test-task/src/product_list.php");
        } else {
            header("Location: http://" . $_SERVER['HTTP_HOST'] . "/product_list.php");
        }
    }
}