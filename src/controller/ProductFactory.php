<?php

require_once __DIR__ . '/../model/Product.php';
require_once __DIR__ . '/../model/CD.php';
require_once __DIR__ . '/../model/Book.php';
require_once __DIR__ . '/../model/Furniture.php';

/**
 * Class ProductFactory
 */
class ProductFactory {

    /**
     * @param string $type
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int ...$params
     * @return Product|null
     */
    public static function getProduct(
        string $type,
        string $sku,
        string  $name,
        float $price,
        int ... $params): ?Product  {

        switch ($type) {
            case 'CD':
                return new CD($sku, $name, $price, $params[0]);
            case 'Book':
                return new Book($sku, $name, $price, $params[0]);
            case 'Furniture':
                return new Furniture($sku, $name, $price, $params[0], $params[1], $params[2]);
        }
        return null;
    }

}