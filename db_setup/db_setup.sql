-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sku` text NOT NULL,
  `name` text NOT NULL,
  `price` double NOT NULL,
  `weight` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `cd`;
CREATE TABLE `cd` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `sku` text NOT NULL,
  `name` text NOT NULL,
  `price` double NOT NULL,
  `size` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `furniture`;
CREATE TABLE `furniture` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sku` text NOT NULL,
  `name` text NOT NULL,
  `price` double NOT NULL,
  `height` int NOT NULL,
  `width` int NOT NULL,
  `length` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- 2020-09-15 06:58:52
