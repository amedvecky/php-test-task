## Simple PHP application

## Env set up

### Prerequisites

* Installed [docker](https://www.docker.com/products/docker-desktop)
* Installed [PHP](https://www.php.net/) (only for local development)

### App installation

 run docker-compose:

```bash
docker-compose build
docker-compose up
```
 app available on http://localhost
 
### App installation for local development
 
#### MySql and adminer containers set up
 
  run docker-compose:
  
```bash
docker-compose -f docker-compose-db.yml up -d
```

 Then possible to run scripts from IDE (tested on IntelliJ IDEA)



