FROM  php:7.4-apache
COPY src/ /var/www/html/
RUN chmod -R 755 /var/www/html/
RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql

